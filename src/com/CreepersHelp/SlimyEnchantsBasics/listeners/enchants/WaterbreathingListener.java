package com.CreepersHelp.SlimyEnchantsBasics.listeners.enchants;

import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.CreepersHelp.SlimyEnchantsAPI.SlimyEnchantsAPI;
import com.CreepersHelp.SlimyEnchantsBasics.SlimyEnchantsBasics;
import com.CreepersHelp.SlimyEnchantsAPI.APIs.EnchantListener;
import com.CreepersHelp.SlimyEnchantsBasics.enchants.Enchants.EnchantTypes;

public class WaterbreathingListener extends EnchantListener {

	public WaterbreathingListener() {
		super(SlimyEnchantsBasics.getPlugin());
	}
	
	@EventHandler
	public void onPlayerMove(PlayerMoveEvent event) {
		if (SlimyEnchantsAPI.getUser(event.getPlayer()).hasEquipmentEnchantment(EnchantTypes.WATER_BREATHING.value()))
			use(SlimyEnchantsAPI.getUser(event.getPlayer()).getEnchantmenLevel(event.getPlayer().getInventory().getArmorContents(), EnchantTypes.WATER_BREATHING.value()), event.getPlayer());
	}

	public void use(int level, LivingEntity paramLivingEntity) {
		paramLivingEntity.addPotionEffect(new PotionEffect(PotionEffectType.WATER_BREATHING, 20 + level * 10, level), true);
	}
}
