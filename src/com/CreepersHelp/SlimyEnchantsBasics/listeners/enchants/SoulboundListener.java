package com.CreepersHelp.SlimyEnchantsBasics.listeners.enchants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.bukkit.Material;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;

import com.CreepersHelp.SlimyEnchantsAPI.SlimyEnchantsAPI;
import com.CreepersHelp.SlimyEnchantsBasics.SlimyEnchantsBasics;
import com.CreepersHelp.SlimyEnchantsBasics.enchants.Enchants.EnchantTypes;

public class SoulboundListener extends com.CreepersHelp.SlimyEnchantsAPI.APIs.EnchantListener {
	
	public SoulboundListener() {
		super(SlimyEnchantsBasics.getPlugin());
	}

	public static boolean removeSoul = false;
	
	public final static HashMap<UUID, List<ItemStack>> items = new HashMap<UUID, List<ItemStack>>();
	
	  public void onPlayerDie(PlayerDeathEvent event)
	  {
		  List<ItemStack> drops = new ArrayList<ItemStack>();
		  for(ItemStack drop : event.getDrops())
		  {
			  if(drop.getType() != Material.AIR)
			  {
				  if (drop.containsEnchantment(EnchantTypes.SOULBOUND.value()))
				  {
					  if (removeSoul) drop.removeEnchantment(EnchantTypes.SOULBOUND.value());
					  drops.add(drop);
					  event.getDrops().remove(event.getDrops().indexOf(drop));
				  }
			  }
		  }
		  items.put(event.getEntity().getUniqueId(), drops);
	  }
	  
	  public void onItemDrop(PlayerDropItemEvent event) {
		  if (SlimyEnchantsAPI.getUser(event.getPlayer()).hasEnchantment(event.getItemDrop().getItemStack(), EnchantTypes.SOULBOUND.value()))
			  event.setCancelled(true);
	  }
	  
	  public void onPlayerRespawn(PlayerRespawnEvent paramPlayerRespawnEvent)
	  {

		  List<ItemStack> drops = items.get(paramPlayerRespawnEvent.getPlayer().getUniqueId());		  
		  for (ItemStack item : drops) {
			  paramPlayerRespawnEvent.getPlayer().getInventory().addItem(item);
			  drops.remove(item);
		  }
		  
		  if (items.containsKey(paramPlayerRespawnEvent.getPlayer().getUniqueId())) items.remove(paramPlayerRespawnEvent.getPlayer().getUniqueId());
	  }
}