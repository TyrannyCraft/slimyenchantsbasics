package com.CreepersHelp.SlimyEnchantsBasics.listeners.enchants;

import java.util.HashMap;
import java.util.Random;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;

import com.CreepersHelp.SlimyEnchantsAPI.SlimyEnchantsAPI;
import com.CreepersHelp.SlimyEnchantsBasics.SlimyEnchantsBasics;
import com.CreepersHelp.SlimyEnchantsAPI.APIs.EnchantListener;
import com.CreepersHelp.SlimyEnchantsAPI.APIs.User;
import com.CreepersHelp.SlimyEnchantsAPI.APIs.events.SlimyPlayerActivityEvent;
import com.CreepersHelp.SlimyEnchantsBasics.enchants.Enchants.EnchantTypes;

public class FlightListener extends EnchantListener {
	
	public final static HashMap<UUID, Integer> playersLocation = new HashMap<UUID, Integer>();
	
	public FlightListener() {
		super(SlimyEnchantsBasics.getPlugin());
	}
	
	@EventHandler   
	public void onPlayerMove(final PlayerMoveEvent event){
		refresh(SlimyEnchantsAPI.getUser(event.getPlayer()));
	}
	
	@EventHandler
	public void onPlayerInactivity(final SlimyPlayerActivityEvent event) {
		if (event.getActivity() == SlimyPlayerActivityEvent.ActivityType.INACTIVE) {
			playersLocation.put(event.getPlayer().getUniqueId(), 
					Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(getListenerPlugin(), new Runnable() {
						@Override
						public void run() {
							refresh(SlimyEnchantsAPI.getUser(event.getPlayer()));
						}
					}, 0L, 20L)
			);
		} else 		if (event.getActivity() == SlimyPlayerActivityEvent.ActivityType.ACTIVE) {
			if (playersLocation.containsKey(event.getPlayer().getUniqueId())) {
				if (playersLocation.get(event.getPlayer().getUniqueId()) != -1 && playersLocation.get(event.getPlayer().getUniqueId()) != null)
					Bukkit.getServer().getScheduler().cancelTask(playersLocation.get(event.getPlayer().getUniqueId()));
				playersLocation.remove(event.getPlayer().getUniqueId());
			}
		}
	}
	
	public void refresh(final User user) {
		try {
			if (user.hasEquipmentEnchantment(EnchantTypes.FLIGHT.value()) || user.getGameMode().equals(GameMode.CREATIVE)) {
	              if (!user.getAllowFlight()) {
	            	  user.sendMessage("You now have flight");
	            	  user.setAllowFlight(true);
	            	  user.setFlying(true);
	              }
				if (user.isFlying() && !user.getGameMode().equals(GameMode.CREATIVE)) {
					for (ItemStack item : user.getInventory().getArmorContents()) {
						if (item == null || item.equals(Material.AIR)) continue;
						
						if (user.checkItemDecay(item)) {
							if (user.removeItem(item))
								user.sendMessage(user.getItemName(item) + ChatColor.RED + " was removed! :'(");
							continue;
						}
						
						//System.err.println(user.getName() + " | " + item.getType().toString() + " | " + user.hasEnchantment(item, EnchantTypes.FLIGHT.value()) );
						
						if (item.getDurability() <= 0 || item.getType().getMaxDurability() <= 0) continue;
						
						int max = item.getType().getMaxDurability() - 5;
						if (item == null || item.equals(Material.AIR)) continue;
						boolean t = (item.getDurability() >= max);
						
						//System.err.println((t) + " | " + item.getDurability() + " | " + max);
						
						if (t)
							user.sendMessage(user.getItemName(item) + ChatColor.RED + " is about to break!");
						
//						user.sendMessage(item.getDurability() + "/" + item.getType().getMaxDurability());
						
						if (new Random().nextInt(100) < 10) {
							if (user.hasEnchantment(item, EnchantTypes.FLIGHT.value())) {
								//System.err.println(user.getName() + " | Decay2?");
								item.setDurability((short)(item.getDurability() + 1));
							} else  continue;
						}
					}
				}
			} else {
				if (user.getAllowFlight()) {
					user.sendMessage("You no longer have flight");
	                  user.setAllowFlight(false);
	                  user.setFlying(false);
				}
			}
		} catch (Exception e) {e.printStackTrace();}
	}
} 
