package com.CreepersHelp.SlimyEnchantsBasics.listeners.enchants;

import java.util.HashMap;

import org.bukkit.Location;
import org.bukkit.enchantments.Enchantment;

import java.util.UUID;

import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerMoveEvent;

import ru.BeYkeRYkt.LightAPI.LightAPI;

import com.CreepersHelp.SlimyEnchantsAPI.SlimyEnchantsAPI;
import com.CreepersHelp.SlimyEnchantsBasics.SlimyEnchantsBasics;
import com.CreepersHelp.SlimyEnchantsAPI.APIs.EnchantListener;
import com.CreepersHelp.SlimyEnchantsAPI.APIs.User;
import com.CreepersHelp.SlimyEnchantsBasics.enchants.Enchants.EnchantTypes;

public class EluminatorListener extends EnchantListener {

	public EluminatorListener() {
		super(SlimyEnchantsBasics.getPlugin());
	}
	
	private final static HashMap<UUID, Location> lights = new HashMap<UUID, Location>();
	
	@EventHandler
	public void onPlayerMove(PlayerMoveEvent event) {
		User user = SlimyEnchantsAPI.getUser(event.getPlayer());
		
		if (lights.containsKey(user.getUniqueId())) LightAPI.deleteLight(lights.get(user.getUniqueId()));
		lights.remove(user.getUniqueId());
		
		Enchantment e = EnchantTypes.ELUMINATOR.value();
		if (user.hasEquipmentEnchantment(e)) {
			int level = user.getEnchantmenLevel(user.getInventory().getArmorContents(), e);
			LightAPI.createLight(user.getEyeLocation(), (level + 0) * 3);
			lights.put(user.getUniqueId(), user.getEyeLocation());
		}
	}
}
