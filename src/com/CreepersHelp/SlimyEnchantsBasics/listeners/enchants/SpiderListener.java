package com.CreepersHelp.SlimyEnchantsBasics.listeners.enchants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerMoveEvent;

import com.CreepersHelp.SlimyEnchantsAPI.SlimyEnchantsAPI;
import com.CreepersHelp.SlimyEnchantsBasics.SlimyEnchantsBasics;
import com.CreepersHelp.SlimyEnchantsAPI.APIs.EnchantListener;
import com.CreepersHelp.SlimyEnchantsBasics.enchants.Enchants.EnchantTypes;

public class SpiderListener extends EnchantListener {

	  Map<String, ArrayList<Block>> vineMap = new HashMap<String, ArrayList<Block>>();
	  ArrayList<Integer> NoClimbBlocks = new ArrayList<Integer>();
	  ArrayList<String> climbingPlayers = new ArrayList<String>();
	  
	
	@SuppressWarnings("deprecation")
	public SpiderListener() {
		super(SlimyEnchantsBasics.getPlugin());
		    NoClimbBlocks.add(Integer.valueOf(Material.THIN_GLASS.getId()));
		    NoClimbBlocks.add(Integer.valueOf(44));
		    NoClimbBlocks.add(Integer.valueOf(126));
		    NoClimbBlocks.add(Integer.valueOf(Material.WOOD_STAIRS.getId()));
		    NoClimbBlocks.add(Integer.valueOf(Material.JUNGLE_WOOD_STAIRS.getId()));
		    NoClimbBlocks.add(Integer.valueOf(Material.BIRCH_WOOD_STAIRS.getId()));
		    NoClimbBlocks.add(Integer.valueOf(Material.SPRUCE_WOOD_STAIRS.getId()));
		    NoClimbBlocks.add(Integer.valueOf(Material.COBBLESTONE_STAIRS.getId()));
		    NoClimbBlocks.add(Integer.valueOf(Material.BRICK_STAIRS.getId()));
		    NoClimbBlocks.add(Integer.valueOf(Material.WOOD_STAIRS.getId()));
		    NoClimbBlocks.add(Integer.valueOf(Material.SMOOTH_STAIRS.getId()));
		    NoClimbBlocks.add(Integer.valueOf(Material.NETHER_BRICK_STAIRS.getId()));
		    NoClimbBlocks.add(Integer.valueOf(Material.SANDSTONE_STAIRS.getId()));
		    NoClimbBlocks.add(Integer.valueOf(Material.FENCE.getId()));
		    NoClimbBlocks.add(Integer.valueOf(Material.FENCE_GATE.getId()));
		    NoClimbBlocks.add(Integer.valueOf(Material.NETHER_FENCE.getId()));
		    NoClimbBlocks.add(Integer.valueOf(Material.LADDER.getId()));
		    NoClimbBlocks.add(Integer.valueOf(Material.VINE.getId()));
		    NoClimbBlocks.add(Integer.valueOf(Material.BED.getId()));
		    NoClimbBlocks.add(Integer.valueOf(Material.BED_BLOCK.getId()));
		    NoClimbBlocks.add(Integer.valueOf(Material.LONG_GRASS.getId()));
		    NoClimbBlocks.add(Integer.valueOf(Material.IRON_FENCE.getId()));
		    NoClimbBlocks.add(Integer.valueOf(Material.SNOW.getId()));
		    NoClimbBlocks.add(Integer.valueOf(Material.SIGN.getId()));
		    NoClimbBlocks.add(Integer.valueOf(Material.LEVER.getId()));
		    NoClimbBlocks.add(Integer.valueOf(Material.TRAP_DOOR.getId()));
		    NoClimbBlocks.add(Integer.valueOf(Material.PISTON_EXTENSION.getId()));
		    NoClimbBlocks.add(Integer.valueOf(Material.PISTON_MOVING_PIECE.getId()));
		    NoClimbBlocks.add(Integer.valueOf(Material.TRIPWIRE_HOOK.getId()));
		    NoClimbBlocks.add(Integer.valueOf(93));
		    NoClimbBlocks.add(Integer.valueOf(94));
		    NoClimbBlocks.add(Integer.valueOf(Material.BOAT.getId()));
		    NoClimbBlocks.add(Integer.valueOf(Material.MINECART.getId()));
		    NoClimbBlocks.add(Integer.valueOf(Material.CAKE.getId()));
		    NoClimbBlocks.add(Integer.valueOf(Material.CAKE_BLOCK.getId()));
		    NoClimbBlocks.add(Integer.valueOf(Material.WATER.getId()));
		    NoClimbBlocks.add(Integer.valueOf(Material.STATIONARY_WATER.getId()));
		    NoClimbBlocks.add(Integer.valueOf(Material.LAVA.getId()));
		    NoClimbBlocks.add(Integer.valueOf(Material.STATIONARY_LAVA.getId()));
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	  public void onPlayerMove(PlayerMoveEvent event)
	  {
		  
		if (!SlimyEnchantsAPI.getUser(event.getPlayer()).hasEquipmentEnchantment(EnchantTypes.SPIDER.value())) return;
		
	    Player player = event.getPlayer();
	    
	    climbingPlayers.add(player.getName());
	    
	    if ((climbingPlayers.contains(player.getName())) && 
	      (player.hasPermission("spider.use")))
	    {
	      BlockFace bf = yawToFace(player.getLocation().getYaw());
	      Block block = player.getLocation().getBlock().getRelative(bf);
	      if (block.getType() != Material.AIR)
	      {
	        for (int i = 0; i < 300; i++)
	        {
	          Block temp = block.getLocation().add(0.0D, i, 0.0D).getBlock();
	          Block opp = player.getLocation().add(0.0D, i, 0.0D).getBlock();
	          Block aboveOpp = opp.getLocation().add(0.0D, 1.0D, 0.0D).getBlock();
	          int counter = 0;
	          for (int k = 0; k < NoClimbBlocks.size(); k++) {
	            if ((temp.getType() != Material.AIR) && (temp.getTypeId() != ((Integer)NoClimbBlocks.get(k)).intValue())) {
	              counter++;
	            }
	          }
	          if ((counter != NoClimbBlocks.size()) || ((opp.getType() != Material.AIR) && (opp.getType() != Material.LONG_GRASS) && (opp.getType() != Material.YELLOW_FLOWER) && (opp.getType() != Material.RED_ROSE))) {
	            break;
	          }
	          if (aboveOpp.getType() == Material.AIR)
	          {
	            player.sendBlockChange(opp.getLocation(), Material.VINE, (byte)0);
	            addVines(player, opp);
	          }
	          player.setFallDistance(0.0F);
	        }
	      }
	      else
	      {
	        for (int i = 0; i < getVines(player).size(); i++) {
	          player.sendBlockChange(((Block)getVines(player).get(i)).getLocation(), Material.AIR, (byte)0);
	        }
	        getVines(player).clear();
	      }
	    }
	  }
	  
	  public ArrayList<Block> getVines(Player player)
	  {
	    if (this.vineMap.containsKey(player.getName())) {
	      return (ArrayList<Block>)this.vineMap.get(player.getName());
	    }
	    ArrayList<Block> temp = new ArrayList<Block>();
	    return temp;
	  }
	  
	  public void setVines(Player player, ArrayList<Block> vines)
	  {
	    this.vineMap.put(player.getName(), vines);
	  }
	  
	  public void addVines(Player player, Block vine)
	  {
	    ArrayList<Block> updated = new ArrayList<Block>();
	    updated = getVines(player);
	    updated.add(vine);
	    setVines(player, updated);
	  }
	  
	  public BlockFace yawToFace(float yaw)
	  {
	    BlockFace[] axis = { BlockFace.SOUTH, BlockFace.WEST, BlockFace.NORTH, BlockFace.EAST };
	    return axis[(java.lang.Math.round(yaw / 90.0F) & 0x3)];
	  }
	
}
