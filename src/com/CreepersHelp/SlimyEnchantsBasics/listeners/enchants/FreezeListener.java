package com.CreepersHelp.SlimyEnchantsBasics.listeners.enchants;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;

import com.CreepersHelp.SlimyEnchantsAPI.SlimyEnchantsAPI;
import com.CreepersHelp.SlimyEnchantsBasics.SlimyEnchantsBasics;
import com.CreepersHelp.SlimyEnchantsBasics.enchants.Enchants.EnchantTypes;

public class FreezeListener extends com.CreepersHelp.SlimyEnchantsAPI.APIs.EnchantListener {

	private final static HashMap<UUID, Long> frozen = new HashMap<UUID, Long> ();
	
	public FreezeListener() {
		super(SlimyEnchantsBasics.getPlugin());
	}
	

	@EventHandler
	public void onPlayerMove(PlayerMoveEvent event) {
		if (event == null || event.getPlayer() == null || event.getPlayer().getUniqueId() == null) return; 
		if (!frozen.containsKey(event.getPlayer().getUniqueId())) return; 
		//System.err.println(System.currentTimeMillis() + " | " + frozen.get(event.getPlayer().getUniqueId()));
	}
	
	@EventHandler
	public void onEntityDamage(EntityDamageByEntityEvent event) {
		if (event.getDamager() instanceof Player && event.getEntity() instanceof Player) {
			Player player = (Player) event.getDamager();
			Player entity = (Player) event.getEntity();
			if (SlimyEnchantsAPI.getUser(player).hasEnchantment(player.getInventory().getItemInHand(), EnchantTypes.FREEZE.value())) {
				int level = player.getItemInHand().getEnchantmentLevel(EnchantTypes.FREEZE.value());
				level += 2;
				frozen.put(entity.getUniqueId(), System.currentTimeMillis() + 20000);
				System.err.println("1Player: " + player.getName() + " froze player " + entity.getName());
				System.err.println(System.currentTimeMillis() + " | " + (System.currentTimeMillis() + 20000));
			}
			
			if (SlimyEnchantsAPI.getUser(entity).hasEquipmentEnchantment(EnchantTypes.FREEZE.value())) {
				int level = SlimyEnchantsAPI.getUser(entity).getEnchantmenLevel(new ItemStack[] {
						entity.getInventory().getHelmet(),
						entity.getInventory().getChestplate(),
						entity.getInventory().getLeggings(),
						entity.getInventory().getBoots(),
				}, EnchantTypes.FREEZE.value());
				level += 2;
				frozen.put(entity.getUniqueId(), System.currentTimeMillis() + 20000);
				System.err.println("2Player: " + entity.getName() + " froze player " + player.getName());
				System.err.println(System.currentTimeMillis() + " | " + (System.currentTimeMillis() + 20000));
			}
		}
	}
	
}
