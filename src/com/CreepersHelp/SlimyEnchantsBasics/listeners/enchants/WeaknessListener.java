package com.CreepersHelp.SlimyEnchantsBasics.listeners.enchants;

import com.CreepersHelp.SlimyEnchantsBasics.SlimyEnchantsBasics;
import com.CreepersHelp.SlimyEnchantsAPI.APIs.EnchantListener;

public class WeaknessListener extends EnchantListener {

	public WeaknessListener() {
		super(SlimyEnchantsBasics.getPlugin());
	}
}
