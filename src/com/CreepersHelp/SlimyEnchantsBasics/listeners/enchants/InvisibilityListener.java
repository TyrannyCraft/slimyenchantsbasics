package com.CreepersHelp.SlimyEnchantsBasics.listeners.enchants;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerMoveEvent;

import com.CreepersHelp.SlimyEnchantsAPI.SlimyEnchantsAPI;
import com.CreepersHelp.SlimyEnchantsBasics.SlimyEnchantsBasics;
import com.CreepersHelp.SlimyEnchantsAPI.APIs.EnchantListener;
import com.CreepersHelp.SlimyEnchantsAPI.APIs.events.SlimyPlayerJoinEvent;
import com.CreepersHelp.SlimyEnchantsBasics.enchants.Enchants.EnchantTypes;
import com.CreepersHelp.SlimyEnchantsBasics.threads.InvisibilityThread;

public class InvisibilityListener extends EnchantListener {
	
	private final static HashMap<UUID, InvisibilityThread> iThreads = new HashMap<UUID, InvisibilityThread>();
	 
	public InvisibilityListener() {
		super(SlimyEnchantsBasics.getPlugin());
	}


	@EventHandler
	public void onPlayerJoin(final SlimyPlayerJoinEvent event) {
		for (Player player : InvisibilityThread.hidePlayers)
			event.getPlayer().hidePlayer(player);
		
		try {
				if(!event.getPlayer().isOnline()) return;
				if (SlimyEnchantsAPI.getUser(event.getPlayer()).hasEquipmentEnchantment(EnchantTypes.INVISIBILITY.value())) {
					if (!iThreads.containsKey(event.getPlayer().getUniqueId())) {
						iThreads.put(event.getPlayer().getUniqueId(), new InvisibilityThread(event.getPlayer().getUniqueId()));
						iThreads.get(event.getPlayer().getUniqueId()).addInvisibility();	
					}
				} else {
					if (iThreads.containsKey(event.getPlayer().getUniqueId())) {
						iThreads.get(event.getPlayer().getUniqueId()).removeInvisibility();
						iThreads.remove(event.getPlayer().getUniqueId()).addInvisibility();
					}
				}	
		} catch (Exception e) { e.printStackTrace();}
	}
	
	public void onPlayerMove(final PlayerMoveEvent event) {
		try {
			Bukkit.getScheduler().scheduleSyncDelayedTask(getListenerPlugin(), new Runnable() {
			@Override
			public void run() {
				if(!event.getPlayer().isOnline()) return;
				if (SlimyEnchantsAPI.getUser(event.getPlayer()).hasEquipmentEnchantment(EnchantTypes.INVISIBILITY.value())) {
					if (!iThreads.containsKey(event.getPlayer().getUniqueId())) {
						iThreads.put(event.getPlayer().getUniqueId(), new InvisibilityThread(event.getPlayer().getUniqueId()));
						iThreads.get(event.getPlayer().getUniqueId()).addInvisibility();	
					}
				} else {
					if (iThreads.containsKey(event.getPlayer().getUniqueId())) {
						iThreads.get(event.getPlayer().getUniqueId()).removeInvisibility();
						iThreads.remove(event.getPlayer().getUniqueId()).addInvisibility();
					}
				}	
			}
		}, 2L);
			
		} catch (Exception e) { e.printStackTrace();}
	}
} 
