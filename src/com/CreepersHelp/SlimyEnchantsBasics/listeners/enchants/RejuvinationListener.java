package com.CreepersHelp.SlimyEnchantsBasics.listeners.enchants;

import com.CreepersHelp.SlimyEnchantsBasics.SlimyEnchantsBasics;
import com.CreepersHelp.SlimyEnchantsAPI.APIs.EnchantListener;

public class RejuvinationListener extends EnchantListener {

	public RejuvinationListener() {
		super(SlimyEnchantsBasics.getPlugin());
	}
}
