package com.CreepersHelp.SlimyEnchantsBasics.listeners.enchants;

import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.CreepersHelp.SlimyEnchantsAPI.SlimyEnchantsAPI;
import com.CreepersHelp.SlimyEnchantsBasics.SlimyEnchantsBasics;
import com.CreepersHelp.SlimyEnchantsAPI.APIs.EnchantListener;
import com.CreepersHelp.SlimyEnchantsBasics.enchants.Enchants.EnchantTypes;

public class DrunkListener extends EnchantListener {

	public DrunkListener() {
		super(SlimyEnchantsBasics.getPlugin());
	}
	
	@EventHandler
	public void onEntityDamage(EntityDamageByEntityEvent event) {
		if (event.getDamager() instanceof Player && event.getEntity() instanceof Player) {
			Player damager = (Player) event.getDamager();
			Player entity = (Player) event.getEntity();
			if (SlimyEnchantsAPI.getUser(damager).hasEnchantment(damager.getInventory().getItemInHand(), EnchantTypes.DRUNK.value()))
				use(damager.getItemInHand().getEnchantmentLevel(EnchantTypes.DRUNK.value()), (LivingEntity)event.getEntity());
			if (SlimyEnchantsAPI.getUser(entity).hasEquipmentEnchantment(EnchantTypes.DRUNK.value()))
				use(SlimyEnchantsAPI.getUser(entity).getEnchantmenLevel(new ItemStack[] {
						entity.getInventory().getHelmet(),
						entity.getInventory().getChestplate(),
						entity.getInventory().getLeggings(),
						entity.getInventory().getBoots(),
				}, EnchantTypes.DRUNK.value()), (LivingEntity)event.getDamager());
		}
	}

	public void use(int level, LivingEntity paramLivingEntity) {
		paramLivingEntity.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 20 + level * 10, level), true);
	}
	
}
