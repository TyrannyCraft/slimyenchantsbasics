package com.CreepersHelp.SlimyEnchantsBasics.listeners.enchants;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockBreakEvent;

import com.CreepersHelp.SlimyEnchantsAPI.SlimyEnchantsAPI;
import com.CreepersHelp.SlimyEnchantsBasics.SlimyEnchantsBasics;
import com.CreepersHelp.SlimyEnchantsAPI.APIs.EnchantListener;
import com.CreepersHelp.SlimyEnchantsBasics.enchants.Enchants.EnchantTypes;


public class AutoSmeltListener extends EnchantListener {

	public AutoSmeltListener() {
		super(SlimyEnchantsBasics.getPlugin());
	}
	
	@EventHandler
	public void onBlockBreak(final BlockBreakEvent paramBlockBreakEvent)
	{
		if (!SlimyEnchantsAPI.getUser(paramBlockBreakEvent.getPlayer()).hasEnchantment(paramBlockBreakEvent.getPlayer().getInventory().getItemInHand(), EnchantTypes.AUTO_SMELT.value()))
			return;
	    
		Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(getListenerPlugin(), new Runnable() {
			
			@Override
			public void run() {
				  paramBlockBreakEvent.getPlayer().sendMessage("Sorry, AutoSmelt is not currently workings");
			}
		}, 1L);
	}	
}
