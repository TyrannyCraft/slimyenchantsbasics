package com.CreepersHelp.SlimyEnchantsBasics.listeners.enchants;

import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.CreepersHelp.SlimyEnchantsAPI.SlimyEnchantsAPI;
import com.CreepersHelp.SlimyEnchantsBasics.SlimyEnchantsBasics;
import com.CreepersHelp.SlimyEnchantsAPI.APIs.EnchantListener;
import com.CreepersHelp.SlimyEnchantsBasics.enchants.Enchants.EnchantTypes;

public class NightVisionListener extends EnchantListener {

	public NightVisionListener() {
		super(SlimyEnchantsBasics.getPlugin());
	}
	
	@EventHandler
	public void onPlayerMove(PlayerMoveEvent event) {
		if (SlimyEnchantsAPI.getUser(event.getPlayer()).hasEquipmentEnchantment(EnchantTypes.NIGHT_VISION.value())) {
			int level = SlimyEnchantsAPI.getUser(event.getPlayer()).getEnchantmenLevel(event.getPlayer().getInventory().getArmorContents(), EnchantTypes.NIGHT_VISION.value());
			event.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION, 120 + level, level , false, false));
		}
	}
}
