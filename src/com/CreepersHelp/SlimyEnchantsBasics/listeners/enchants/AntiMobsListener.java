package com.CreepersHelp.SlimyEnchantsBasics.listeners.enchants;

import org.bukkit.entity.Ghast;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityTargetEvent;
import org.bukkit.inventory.ItemStack;

import com.CreepersHelp.SlimyEnchantsAPI.SlimyEnchantsAPI;
import com.CreepersHelp.SlimyEnchantsBasics.SlimyEnchantsBasics;
import com.CreepersHelp.SlimyEnchantsAPI.APIs.EnchantListener;
import com.CreepersHelp.SlimyEnchantsBasics.enchants.Enchants.EnchantTypes;

public class AntiMobsListener extends EnchantListener {

	public AntiMobsListener() {
		super(SlimyEnchantsBasics.getPlugin());
	}
	
	@EventHandler
    public void onEntityTarget(EntityTargetEvent event){
		if (event.getTarget() instanceof Player){
			Player player = ((Player)event.getTarget());
			ItemStack[] items = new ItemStack[] {
					player.getInventory().getItemInHand(),
					player.getInventory().getHelmet(),
					player.getInventory().getChestplate(),
					player.getInventory().getLeggings(),
					player.getInventory().getBoots(),
					
			};
			
			if (!SlimyEnchantsAPI.getUser(player).hasEnchantment(items, EnchantTypes.ANTI_MOBS.value())) return;
			if (event.getEntity() instanceof Monster || event.getEntity() instanceof Ghast || event.getEntity() instanceof org.bukkit.entity.LightningStrike || event.getEntity() instanceof org.bukkit.entity.MagmaCube || event.getEntity() instanceof org.bukkit.entity.Guardian) {
				int level = SlimyEnchantsAPI.getUser(player).getEnchantmenLevel(items, EnchantTypes.ANTI_MOBS.value()) * 20;
				int random = new java.util.Random().nextInt(100);
				//System.err.println(random + " | " + level);
				if (random <= level) {
					event.setCancelled(true);
					//System.err.println("Cancelled mob for player: "+player.getName());
				}
			}
		}
	}
	
}
