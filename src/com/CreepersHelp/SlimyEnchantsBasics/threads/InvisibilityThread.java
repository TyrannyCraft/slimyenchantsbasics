package com.CreepersHelp.SlimyEnchantsBasics.threads;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.CreepersHelp.SlimyEnchantsBasics.enchants.Enchants.EnchantTypes;
import com.CreepersHelp.SlimyEnchantsBasics.utils.GhostManager;


public class InvisibilityThread {
	
	private final UUID _uuid;
	private final static GhostManager ghostFactory = new GhostManager(EnchantTypes.INVISIBILITY.value().getListener().getListenerPlugin());
	public final static ArrayList<Player> hidePlayers = new ArrayList<Player>(); 
	private int taskID = 0;	

	public InvisibilityThread(UUID uuid) {
		_uuid = uuid;
		
		taskID = Bukkit.getScheduler().scheduleSyncRepeatingTask(EnchantTypes.INVISIBILITY.value().getListener().getListenerPlugin(), new Runnable() {
			@Override
			public void run() {
				try {
					if (!getPlayer().isOnline()) {
						removeInvisibility();
					    Bukkit.getScheduler().cancelTask(taskID);
						return;
					}
				} catch (Exception e) {
					Bukkit.getScheduler().cancelTask(taskID);
					return;
				}
				
				if (containsEnchant(getPlayer()))
					addInvisibility();
			}
		}, 0L, 2L);
	}
	
	public final Player getPlayer() {
		return Bukkit.getPlayer(_uuid);
	}
	
	public final OfflinePlayer[] getInvisiblePlayers() {
		return ghostFactory.getGhosts();
	} 
	
 
	public final void addInvisibility() {
		
		if (!(getLevel() > 0)) return;
		
		if (getLevel() <= 1)
			ghostFactory.setGhost(getPlayer(), true);
	    if (getLevel() >= 2) {
	    	hidePlayers.add(getPlayer());
	    	for (Player player : Bukkit.getOnlinePlayers()) 
	    		player.hidePlayer(getPlayer());
	    }
	}
	
	public final boolean isHidden() {
		return hidePlayers.contains(getPlayer());
	}
	
	public final static boolean containsEnchant(Player player) {
		ArrayList<ItemStack> contents = new ArrayList<ItemStack>();
		contents.addAll(Arrays.asList(player.getInventory().getArmorContents()));
		
		boolean haz = false;
		
		for (ItemStack item : contents) {
			if (item.containsEnchantment(EnchantTypes.INVISIBILITY.value())) 
				haz = true;
		}
		
		return haz;
	}
	
	private final int getLevel() {
		int lvl = 0;
		
		Enchantment ench = EnchantTypes.INVISIBILITY.value();
		
		for (ItemStack item : getPlayer().getInventory().getArmorContents()) 
			if (item.containsEnchantment(ench))
				if (item.getEnchantmentLevel(ench) > lvl)
					lvl = item.getEnchantmentLevel(ench);					
		
		return lvl;
	}
	
	public final void removeInvisibility() {
		hidePlayers.remove(getPlayer());
		for (Player player : Bukkit.getOnlinePlayers())
			player.showPlayer(getPlayer());
		ghostFactory.setGhost(getPlayer(), false);
	}
	
	public final boolean isPlayerInvisible() {
		return ghostFactory.isGhost(getPlayer());
	}
	
} 