package com.CreepersHelp.SlimyEnchantsBasics.utils;

import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.NameTagVisibility;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

public class HidePlayerTag {

    private final static Scoreboard scoreboard = Bukkit.getServer().getScoreboardManager().getNewScoreboard();
    private final static String teamName = "HideTagAPI" + new Random().nextInt(10) + "" + new Random().nextInt(10) + "" + new Random().nextInt(10) + "";

    public  HidePlayerTag() {
        createTeam(scoreboard);
    }
    
    private static void createTeam(Scoreboard scoreboard) {
        Team team = scoreboard.registerNewTeam(teamName);
        team.setNameTagVisibility(NameTagVisibility.NEVER);
    }

    public static void hideTag(Player player) {
        if (player.getScoreboard() != null) {
            if (player.getScoreboard().getTeam(teamName) != null) {
                player.getScoreboard().getTeam(teamName).addPlayer(player);
            } else {
                createTeam(player.getScoreboard());
                player.getScoreboard().getTeam(teamName).addPlayer(player);
            }
        } else {
            player.setScoreboard(scoreboard);
            player.getScoreboard().getTeam(teamName).addPlayer(player);
        }
    }

    public static void showTag(Player player) {
        if (player.getScoreboard() != null) {
            if (player.getScoreboard().getTeam(teamName) != null) {
                player.getScoreboard().getTeam(teamName).removePlayer(player);
            }
        }
    }

    public static Scoreboard getPluginScoreboard() {
        return scoreboard;
    }
}