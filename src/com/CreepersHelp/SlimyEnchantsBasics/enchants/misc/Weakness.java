package com.CreepersHelp.SlimyEnchantsBasics.enchants.misc;

import org.bukkit.enchantments.EnchantmentTarget;

import com.CreepersHelp.SlimyEnchantsAPI.APIs.CustomEnchantment;
import com.CreepersHelp.SlimyEnchantsAPI.APIs.EnchantListener;

public class Weakness extends CustomEnchantment {

	private final int _chance;
	
	public Weakness(EnchantListener listener, boolean enchantmentTable, int chance) {
		super(listener, enchantmentTable);
		_chance = chance;
	}

	@Override
	public int getRemovalOtherChance() {
		return 0;
	}

	@Override
	public int getEnchantmentChance() {
		return _chance;
	}

	@Override
	public int getMaxLevel() {
		return 4;
	}

	@Override
	public EnchantmentTarget getItemTarget() {
		return EnchantmentTarget.ALL;	
	}

	@Override
	public String getDesctiption() {
		// TODO Auto-generated method stub
		return null;
	}

}
