package com.CreepersHelp.SlimyEnchantsBasics.enchants.misc;

import org.bukkit.enchantments.EnchantmentTarget;

import com.CreepersHelp.SlimyEnchantsAPI.APIs.CustomEnchantment;
import com.CreepersHelp.SlimyEnchantsAPI.APIs.EnchantListener;

public class Drunk extends CustomEnchantment {

	private final int _chance;
	
	public Drunk(EnchantListener listener, boolean enchantmentTable, int chance) {
		super(listener, enchantmentTable);
		_chance = chance;
	}

	@Override
	public int getRemovalOtherChance() {
		return 0;
	}

	@Override
	public int getEnchantmentChance() {
		return _chance;
	}

	@Override
	public int getMaxLevel() {
		return 4;
	}

	@Override
	public EnchantmentTarget getItemTarget() {
		return EnchantmentTarget.ALL;	
	}

	@Override
	public String getDesctiption() {
		return "Gives your enemy the nausea effect";
	}

}
