package com.CreepersHelp.SlimyEnchantsBasics.enchants.tools;

import org.bukkit.enchantments.EnchantmentTarget;

import com.CreepersHelp.SlimyEnchantsAPI.APIs.CustomEnchantment;
import com.CreepersHelp.SlimyEnchantsAPI.APIs.EnchantListener;

public class SilkSpawner extends CustomEnchantment {
	
	private final int _chance;
	
	public SilkSpawner(EnchantListener listener, boolean enchantmentTable, int chance) {
		super(listener, enchantmentTable);
		_chance = chance;
	}
	
	@Override
	public int getRemovalOtherChance() {
		return 0;
	}

	@Override
	public int getEnchantmentChance() {
		return _chance;
	}

	@Override
	public int getMaxLevel() {
		return 1;
	}

	@Override
	public EnchantmentTarget getItemTarget() {
		return EnchantmentTarget.TOOL;
	}

	@Override
	public String getDesctiption() {
		return "Mine a spawner!";
	}

}
