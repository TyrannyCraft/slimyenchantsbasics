package com.CreepersHelp.SlimyEnchantsBasics.enchants.tools;

import org.bukkit.enchantments.EnchantmentTarget;

import com.CreepersHelp.SlimyEnchantsAPI.APIs.CustomEnchantment;
import com.CreepersHelp.SlimyEnchantsAPI.APIs.EnchantListener;

public class AutoSmelt extends CustomEnchantment {

	private final int _chance;
	
	public AutoSmelt(EnchantListener listener, boolean enchantmentTable, int chance) {
		super(listener, enchantmentTable);
		_chance = chance;
	}

	@Override
	public int getMaxLevel() {
		return 1;
	}

	@Override
	public EnchantmentTarget getItemTarget() {
		return EnchantmentTarget.TOOL;
	}

	@Override
	public int getRemovalOtherChance() {
		return 4;
	}

	@Override
	public int getEnchantmentChance() {
		return _chance;
	}

	@Override
	public String getDesctiption() {
		return "When mining ores; gives you ingot rather than ore";
	}
}

/**

[SPOILER="Example CustomEnchantment"]
[CODE]import org.bukkit.enchantments.EnchantmentTarget;
import com.CreepersHelp.SlimyEnchantsAPI.APIs.CustomEnchantment;
import com.CreepersHelp.SlimyEnchantsAPI.APIs.EnchantListener;
public class Test extends CustomEnchantment {
    private final int _chance;
   
    public Test(EnchantListener listener, boolean enchantmentTable, int chance) {
        super(listener, enchantmentTable); // The listener that controls the enchantment and the boolean of allowing use of enchanting table
        _chance = chance; // The chance of receiving in enchanting table
    }
    @Override
    public int getMaxLevel() {
        return 1; // The max Enchantment level
    }
    @Override
    public EnchantmentTarget getItemTarget() {
        return EnchantmentTarget.TOOL;
    }
    @Override
    public int getRemovalOtherChance() {
        return 4; // The chance of removing other enchantments while enchanting in enchantment table
    }
    @Override
    public int getEnchantmentChance() {
        return _chance; // The chance of receiving in enchanting table
    }
    @Override
    public String getDesctiption() {
        return "Test enchant :)"; // The description of the enchantment, used for booklet SlimyEnchantsAPI.getEnchantsBooklet();
    }
}[/CODE]
[/SPOILER]


**/
