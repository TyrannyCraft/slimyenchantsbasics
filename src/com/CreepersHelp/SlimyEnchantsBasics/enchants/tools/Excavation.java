package com.CreepersHelp.SlimyEnchantsBasics.enchants.tools;

import org.bukkit.enchantments.EnchantmentTarget;

import com.CreepersHelp.SlimyEnchantsAPI.APIs.CustomEnchantment;
import com.CreepersHelp.SlimyEnchantsAPI.APIs.EnchantListener;

public class Excavation extends CustomEnchantment {
	
	private final int _chance;
	
	public Excavation(EnchantListener listener, boolean enchantmentTable, int chance) {
		super(listener, enchantmentTable);
		_chance = chance;
	}
	
	@Override
	public int getRemovalOtherChance() {
		return 0;
	}

	@Override
	public int getEnchantmentChance() {
		return _chance;
	}

	@Override
	public int getMaxLevel() {
		return 2;
	}

	@Override
	public EnchantmentTarget getItemTarget() {
		return EnchantmentTarget.TOOL;
	}

	@Override
	public String getDesctiption() {
		return "Faster mining; Dig out a 3x3 area";
	}

}
