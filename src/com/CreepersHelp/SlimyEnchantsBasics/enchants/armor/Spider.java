package com.CreepersHelp.SlimyEnchantsBasics.enchants.armor;

import org.bukkit.enchantments.EnchantmentTarget;

import com.CreepersHelp.SlimyEnchantsAPI.APIs.EnchantListener;


public class Spider 
	extends com.CreepersHelp.SlimyEnchantsAPI.APIs.CustomEnchantment {

	int _chance;
	
	public Spider(EnchantListener listener, boolean enchantmentTable, int chance) {
		super(listener, enchantmentTable);
		_chance = chance;
	}

	@Override
	public int getMaxLevel() {
		return 1;
	}

	@Override
	public EnchantmentTarget getItemTarget() {
		return EnchantmentTarget.ARMOR;
	}
	
	@Override
	public int getRemovalOtherChance() {
		return 0;
	}

	@Override
	public int getEnchantmentChance() {
		return _chance;
	}

	@Override
	public String getDesctiption() {
		return "Climb walls";
	}
}