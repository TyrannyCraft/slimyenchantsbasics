package com.CreepersHelp.SlimyEnchantsBasics.enchants.armor;

import org.bukkit.enchantments.EnchantmentTarget;

import com.CreepersHelp.SlimyEnchantsAPI.APIs.CustomEnchantment;
import com.CreepersHelp.SlimyEnchantsAPI.APIs.EnchantListener;


public class SpeedBoost extends CustomEnchantment {

	private int _chance;

	public SpeedBoost(EnchantListener listener, boolean enchantmentTable, int chance) {
		super(listener, enchantmentTable);
		this._chance = chance; 
	}

	@Override
	public int getMaxLevel() {
		return 5;
	}

	@Override
	public EnchantmentTarget getItemTarget() {
		return EnchantmentTarget.ARMOR;
	}

	@Override
	public int getEnchantmentChance() {
		return _chance;
	}

	@Override
	public int getRemovalOtherChance() {
		return 0;
	}

	@Override
	public String getDesctiption() {
		return "Move faster!";
	}

}
