package com.CreepersHelp.SlimyEnchantsBasics.enchants.armor;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.enchantments.EnchantmentTarget;

import com.CreepersHelp.SlimyEnchantsAPI.APIs.EnchantListener;

public class Flight 
	extends com.CreepersHelp.SlimyEnchantsAPI.APIs.CustomEnchantment {

	int _chance;
	
	public Flight(EnchantListener listener, boolean enchantmentTable, int chance) {
		super(listener, enchantmentTable);
		_chance = chance;
	}

	public static List<UUID> playersWithFlight = new ArrayList<UUID>();
	
	@Override
	public int getMaxLevel() {
		return 3;
	}

	@Override
	public EnchantmentTarget getItemTarget() {
		return EnchantmentTarget.ARMOR;
	}
	
	@Override
	public int getRemovalOtherChance() {
		return 1;
	}

	@Override
	public int getEnchantmentChance() {
		return _chance;
	}

	@Override
	public String getDesctiption() {
		return "Allows you to fly";
	}
}