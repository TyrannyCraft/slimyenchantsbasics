package com.CreepersHelp.SlimyEnchantsBasics.enchants.armor;

import org.bukkit.enchantments.EnchantmentTarget;

import com.CreepersHelp.SlimyEnchantsAPI.APIs.CustomEnchantment;
import com.CreepersHelp.SlimyEnchantsAPI.APIs.EnchantListener;

/**
 * Renders the player in a ghost like state.
 * @author nadams
 *
 */
public class Invisibility extends CustomEnchantment { 

		int _chance;
	
	public Invisibility(EnchantListener listener, boolean enchantmentTable, int chance) {
		super(listener, enchantmentTable);
		_chance = chance;
		
	}

	@Override
	public int getMaxLevel() {
		return 2;
	}

	@Override
	public EnchantmentTarget getItemTarget() {
		return EnchantmentTarget.ARMOR;
	}

	@Override
	public int getRemovalOtherChance() {
		return 0;
	}

	@Override
	public int getEnchantmentChance() {
		return _chance; 
	}

	@Override
	public String getDesctiption() {
		return "Turn invisible";
	}
}
