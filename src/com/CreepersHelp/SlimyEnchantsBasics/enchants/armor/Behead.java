package com.CreepersHelp.SlimyEnchantsBasics.enchants.armor;

import org.bukkit.enchantments.EnchantmentTarget;

import com.CreepersHelp.SlimyEnchantsAPI.APIs.CustomEnchantment;
import com.CreepersHelp.SlimyEnchantsAPI.APIs.EnchantListener;

public class Behead
	extends CustomEnchantment
{
	  
	private int _chance;

	public Behead(EnchantListener listener, boolean enchantmentTable, int chance) {
		super(listener, enchantmentTable);
		_chance = chance;
	}

	public EnchantmentTarget getItemTarget() {
	    return EnchantmentTarget.WEAPON;
	}
	  
	public int getMaxLevel() {
		return 3;
	}

	public int getStartLevel() {
		return 1;
	}

	@Override
	public int getRemovalOtherChance() {
		return 0;
	}

	@Override
	public int getEnchantmentChance() {
		return _chance;
	}

	@Override
	public String getDesctiption() {
		// TODO Auto-generated method stub
		return null;
	}
}
