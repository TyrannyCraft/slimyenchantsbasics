package com.CreepersHelp.SlimyEnchantsBasics.enchants.wepon;

import org.bukkit.enchantments.EnchantmentTarget;

import com.CreepersHelp.SlimyEnchantsAPI.APIs.CustomEnchantment;
import com.CreepersHelp.SlimyEnchantsAPI.APIs.EnchantListener;

public class Slowness extends CustomEnchantment {

	private final int _chance;
	
	public Slowness(EnchantListener listener, boolean enchantmentTable, int chance) {
		super(listener, enchantmentTable);
		_chance = chance;
	}

	@Override
	public int getRemovalOtherChance() {
		return 0;
	}

	@Override
	public int getEnchantmentChance() {
		return _chance;
	}

	@Override
	public int getMaxLevel() {
		return 1;
	}

	@Override
	public EnchantmentTarget getItemTarget() {
		return EnchantmentTarget.ALL;
	}

	@Override
	public String getDesctiption() {
		return "Give your enemy slowness";
	}
}
