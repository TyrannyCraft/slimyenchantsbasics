package com.CreepersHelp.SlimyEnchantsBasics.enchants.wepon;

import org.bukkit.enchantments.EnchantmentTarget;

import com.CreepersHelp.SlimyEnchantsAPI.APIs.CustomEnchantment;
import com.CreepersHelp.SlimyEnchantsAPI.APIs.EnchantListener;

public class Blindness extends CustomEnchantment {
	private final int _chance;
	public Blindness(EnchantListener listener, boolean enchantmentTable, int chance) {
		super(listener, enchantmentTable);
		_chance = chance;
	}

	@Override
	public int getRemovalOtherChance() {
		return 1;
	}

	@Override
	public int getEnchantmentChance() {
		return _chance;
	}

	@Override
	public int getMaxLevel() {
		return 3;
	}

	@Override
	public EnchantmentTarget getItemTarget() {
		return EnchantmentTarget.WEAPON;
	}

	@Override
	public String getDesctiption() {
		return "Blinds enemy when attacking";
	}
}