package com.CreepersHelp.SlimyEnchantsBasics.enchants.wepon;

import org.bukkit.enchantments.EnchantmentTarget;

import com.CreepersHelp.SlimyEnchantsAPI.APIs.EnchantListener;

public class Venom  extends com.CreepersHelp.SlimyEnchantsAPI.APIs.CustomEnchantment {

	int _chance;
	
	public Venom(EnchantListener listener, boolean enchantmentTable, int chance) {
		super(listener, enchantmentTable);
		_chance = chance;
	}

	@Override
	public int getRemovalOtherChance() {
		return 0;
	}

	@Override
	public int getEnchantmentChance() {
		return _chance;
	}
	
	@Override
	public int getMaxLevel() {
		return 2;
	}

	@Override
	public EnchantmentTarget getItemTarget() {
		return EnchantmentTarget.WEAPON;
	}

	@Override
	public String getDesctiption() {
		return "Poison your enemy";
	}

}
