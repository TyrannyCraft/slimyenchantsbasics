package com.CreepersHelp.SlimyEnchantsBasics.enchants;

import java.util.List;

import com.CreepersHelp.SlimyEnchantsAPI.APIs.CustomEnchantment;
import com.CreepersHelp.SlimyEnchantsAPI.SlimyEnchantsAPI;
import com.CreepersHelp.SlimyEnchantsBasics.enchants.armor.*;
import com.CreepersHelp.SlimyEnchantsBasics.enchants.misc.*;
import com.CreepersHelp.SlimyEnchantsBasics.enchants.tools.*;
import com.CreepersHelp.SlimyEnchantsBasics.enchants.wepon.*;
import com.CreepersHelp.SlimyEnchantsBasics.listeners.enchants.*;

public class Enchants {
	
	public static enum EnchantTypes {
		//-- Please keep in alphabetical order. --\\ 
		//
		AUTO_SMELT(new AutoSmelt(new AutoSmeltListener(), true, 100)), //--------------------WIP
		ANTI_MOBS(new AntiMobs(new AntiMobsListener(), true, 100)), //-----------------------Done
		//
		BLINDNESS(new Blindness(new BlindnessListener(), true, 100)), //---------------------Done
//		BEHEADING(new Behead(new BeheadListener(), true, 100)), //---------------------------WIP (Postponed)
		//
		DRUNK(new Drunk(new DrunkListener(), true, 100)), //---------------------------------Done
		//
		ELUMINATOR(new Eluminator(new EluminatorListener(), true, 100)), //------------------Done
//		EXCAVATION(new Excavation(new ExcavationListener(), true, 100)), //------------------WIP
		//
		FLIGHT(new Flight(new FlightListener(), true, 100)), //------------------------------Done
		FREEZE(new Freeze(new FreezeListener(), true, 100)), //------------------------------Done
		//
//		INSTABILITY(new Instability(new InstabilityListener(), true, 100)), //---------------WIP
		INVISIBILITY(new Invisibility(new InvisibilityListener(), true, 100)), //------------Done
		//
		JUMP_BOOST(new JumpBoost(new JumpBoostListener(), true, 100)), //--------------------Done
		//
//		LIFE_LEECH(new LifeLeech(new LifeLeechListener(), true, 100)), //--------------------WIP
//		LUCKY(new Lucky(new LuckyListener(), true, 100)), //---------------------------------WIP
		//
		NIGHT_VISION(new NightVision(new NightVisionListener(), true, 100)), //--------------Done
		//
//		QUAKE(new Quake(new QuakeListener(), true, 100)), //---------------------------------WIP
		//
//		REGAIN(new Regain(new RegainListener(), true, 100)), //------------------------------WIP
		REGENERATION(new Regeneration(new RegenerationListener(), true, 100)), //------------Done
//		REINFORCED(new Reinforced(new ReinforcedListener(), true, 100)), //------------------WIP
//		REJUVINATION(new Rejuvination(new RejuvinationListener(), true, 100)), //------------WIP
		//
		SILK_SPAWNER(new SilkSpawner(new SilkSpawnerListener(), true, 100)), //------------------------Done
		SLOWNESS(new Slowness(new SlownessListener(), true, 100)), //------------------------Done
		SOULBOUND(new Soulbound(new SoulboundListener(), true, 100)), //---------------------Done
		SPEED_BOOST(new SpeedBoost(new SpeedBoostListener(), true, 100)), //-----------------Done
		SPIDER(new Spider(new SpiderListener(), true, 100)), //------------------------------Done
		//
		VENOM(new Venom(new VenomListener(), true, 100)), //---------------------------------WIP
		//
		WATER_BREATHING(new Waterbreathing(new WaterbreathingListener(), true, 100)), // ----Done
//		WEAKNESS(new Weakness(new WeaknessListener(), true, 100)), //------------------------WIP
		WITHERING(new Withering(new WitheringListener(), true, 100)), //---------------------Done
		//
		;
		
		private final CustomEnchantment value;
		
		private EnchantTypes(CustomEnchantment e) {
			this.value = e;
		}

		public CustomEnchantment value() {
			return value;
		}
	}
	
	  public static List<CustomEnchantment> getAllEnchants()
	  {
		  return SlimyEnchantsAPI.getEnchantsList();
	  }
}
