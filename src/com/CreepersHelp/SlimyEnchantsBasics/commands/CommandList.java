package com.CreepersHelp.SlimyEnchantsBasics.commands;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.CreepersHelp.SlimyEnchantsAPI.APIs.CustomEnchantment;
import com.CreepersHelp.SlimyEnchantsAPI.SlimyEnchantsAPI;

public class CommandList
  extends Commands
{
  public boolean onCommand(CommandSender paramCommandSender, Command paramCommand, String paramString, String[] paramArrayOfString)
  {
    if ((paramArrayOfString[0].equalsIgnoreCase("list")) && (paramCommandSender.hasPermission("enchantsplus.commands.help")))
    {
      Player localPlayer = (Player)paramCommandSender;
      showEnchants(localPlayer);
      return true;
    }
    return false;
  }
  
  public static void showEnchants(Player paramPlayer)
  {
    paramPlayer.sendMessage(ChatColor.AQUA + "=========================================");
    List<String> enchants = new ArrayList<String>();
    
    for (CustomEnchantment ce : SlimyEnchantsAPI.getEnchantsList()) { 
    	
        if ((ce.getName().contains("Instability")) || (ce.getName().contains("Heavy"))) {
            enchants.add(ChatColor.GOLD + ce.getName() + ChatColor.AQUA);
          } else {
            enchants.add(ChatColor.GREEN + ce.getName() + ChatColor.AQUA);
          }
    	
    	enchants.add(ce.getName());
    }
    
    paramPlayer.sendMessage(ChatColor.AQUA + "=========================================");
  }
}
