package com.CreepersHelp.SlimyEnchantsBasics.commands;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.CreepersHelp.SlimyEnchantsAPI.SlimyEnchantsAPI;

public class CommandHelp
	extends Commands   {
	public boolean onCommand(CommandSender paramCommandSender, Command paramCommand, String paramString, String[] paramArrayOfString) {	
		if ((paramArrayOfString[0].equalsIgnoreCase("help")) && (paramCommandSender.hasPermission("enchantsplus.commands.help"))) {
			Player localPlayer = (Player)paramCommandSender;
			showCommands(localPlayer);
			return true;
		}
		return false;
	}

	public static void showCommands(Player paramPlayer) {
		paramPlayer.sendMessage(ChatColor.AQUA + "=========================================");
		if (getAvailableCmds(paramPlayer) == null) {
			paramPlayer.sendMessage("No perms :}");
		} else {
			for (String str : getAvailableCmds(paramPlayer)) {
				paramPlayer.sendMessage(str);
			}
		}
		paramPlayer.sendMessage(ChatColor.AQUA + "=========================================");
	}

	public static List<String> getAvailableCmds(Player paramPlayer) {
		ArrayList<String> localArrayList = new ArrayList<String>();
		
		if (SlimyEnchantsAPI.permissions.hasPermissions(paramPlayer, "enchantsplus.commands.enchants.enchant")) 
			localArrayList.add(ChatColor.GOLD + "(" + ChatColor.GREEN + "/en enchant|en <Enchant name> [Enchant level]" + ChatColor.GOLD + ")" + ChatColor.DARK_GREEN + " - enchants an item the specified player is holding in hand");

		if (SlimyEnchantsAPI.permissions.hasPermissions(paramPlayer, "enchantsplus.commands.list")) 
			localArrayList.add(ChatColor.GOLD + "(" + ChatColor.GREEN + "/en list" + ChatColor.GOLD + ")" + ChatColor.DARK_GREEN + " - List all enchants. "/*Red color means they're disabled"*/);
		
		if (localArrayList.size() == 0) 
			return null;
		
		return localArrayList;
	}
}
