package com.CreepersHelp.SlimyEnchantsBasics.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack; 
import org.bukkit.inventory.meta.ItemMeta;

import com.CreepersHelp.SlimyEnchantsAPI.APIs.CustomEnchantment;
import com.CreepersHelp.SlimyEnchantsAPI.SlimyEnchantsAPI;
import com.CreepersHelp.SlimyEnchantsBasics.enchants.Enchants;

public class CommandApplyEnchant extends Commands {

	@Override
	public boolean onCommand(CommandSender paramCommandSender, Command paramCommand, String paramString, String[] paramArrayOfString) {
		Player psender = null;
		if (paramCommandSender instanceof Player) psender = (Player) paramCommandSender; 
		else return true;
	    if ((paramArrayOfString[0].equalsIgnoreCase("enchant")) || (paramArrayOfString[0].equalsIgnoreCase("en")))
	    {
	    	if (!SlimyEnchantsAPI.permissions.hasPermissions(psender, "enchantsplus.commands.enchants.enchant")) psender.sendMessage(SlimyEnchantsAPI.fixColors("&cNot allowed to enchant"));
	    	
	    	for (CustomEnchantment ce : Enchants.getAllEnchants()) { 
	    		if (ce.getName().equalsIgnoreCase(paramArrayOfString[1])) applyEnchant(psender, psender.getItemInHand(),ce, ce.getStartLevel());
	    	}
	    }
	    return false;
	}

	public static void applyEnchant(Player psender, ItemStack hand, CustomEnchantment ce, int level) {
		if (level > ce.getMaxLevel())
			psender.sendMessage(SlimyEnchantsAPI.fixColors(ChatColor.RED + "The max enchant level for this is: " + ce.getMaxLevel() + "\n[1-"+ce.getMaxLevel()+"]"));
		else if (level <= 0) 
			psender.sendMessage(SlimyEnchantsAPI.fixColors(ChatColor.RED + "The min enchant level for this is: 1" + "\n[1-"+ce.getMaxLevel()+"]"));
//		String lvl = "NaN", enchantName = ce.getName(), numeral = com.CreepersHelp.utils.RomanNumerals.getNumeral(level), fullEnchantName = enchantName  + " " + numeral;
	
		ItemMeta im = hand.getItemMeta();
		im.addEnchant(ce, level, true);
		hand.setItemMeta(im);
	}
}
