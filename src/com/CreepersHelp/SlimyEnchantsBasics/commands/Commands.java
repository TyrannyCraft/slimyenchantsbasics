package com.CreepersHelp.SlimyEnchantsBasics.commands;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.CreepersHelp.SlimyEnchantsAPI.APIs.CustomEnchantment;
import com.CreepersHelp.SlimyEnchantsAPI.SlimyEnchantsAPI;
import com.CreepersHelp.SlimyEnchantsBasics.enchants.Enchants;

public class Commands
	implements CommandExecutor {
	
	public static List<Commands> commands = new ArrayList<Commands>();

	public Commands() {

	}

	@Override
	public boolean onCommand(CommandSender paramCommandSender, Command paramCommand, String paramString, String[] paramArrayOfString) {
		Player psender = null;
		if (paramCommandSender instanceof Player) psender = (Player) paramCommandSender; 
		else return true;
	    if ((paramArrayOfString[0].equalsIgnoreCase("enchant")) || (paramArrayOfString[0].equalsIgnoreCase("en")))
	    {
	    	if (!SlimyEnchantsAPI.permissions.hasPermissions(psender, "enchantsplus.commands.enchants.enchant")) psender.sendMessage(SlimyEnchantsAPI.fixColors("&cNot allowed to enchant"));
	    	
	    	for (CustomEnchantment ce : Enchants.getAllEnchants()) { 
	    		if (ce.getName().equalsIgnoreCase(paramArrayOfString[1])) CommandApplyEnchant.applyEnchant(psender, psender.getItemInHand(),ce, ce.getStartLevel());
	    	}
	    }
	    return false;
	}
	
	public static void registerCommands() {	
		commands.add(new CommandApplyEnchant());
		commands.add(new CommandHelp());
	    commands.add(new CommandList());
	  }
}
