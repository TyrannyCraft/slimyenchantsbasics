package com.CreepersHelp.SlimyEnchantsBasics;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import com.CreepersHelp.SlimyEnchantsAPI.SlimyEnchantsAPI;
import com.CreepersHelp.SlimyEnchantsBasics.enchants.Enchants.EnchantTypes;

public class SlimyEnchantsBasics extends JavaPlugin {

	public static String sName = ChatColor.AQUA + "[" + ChatColor.GREEN + "Slimy" + ChatColor.AQUA + "Enchants" + ChatColor.YELLOW + "Utils] " + ChatColor.GRAY;
	private static SlimyEnchantsBasics plugin;
	
	@Override
	public void onEnable() {
		
		plugin = this;
		
		Bukkit.getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
			
			@Override
			public void run() {
				
				SlimyEnchantsAPI.logger.info(sName + "Enabling");
				
				for (EnchantTypes eType : EnchantTypes.values())
					SlimyEnchantsAPI.addEnchantment(eType.value());
				
				SlimyEnchantsAPI.logger.info(sName + "Enabled");
			}
		}, 10L);
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)) return true;
			if (args.length == 0) help((Player) sender);
			if (args.length == 1 && args[0].equalsIgnoreCase("book")) {
				if (!((Player)sender).getInventory().contains(SlimyEnchantsAPI.getEnchantsBooklet()))
					((Player)sender).getInventory().addItem(SlimyEnchantsAPI.getEnchantsBooklet());
				else ((Player)sender).sendMessage(sName + ChatColor.GREEN + "You already have book!");
				return true;
			}
			if (args.length > 1)
				if (args[0].equalsIgnoreCase("enchant"))
					if (SlimyEnchantsAPI.permissions.hasPermissions(((Player)sender), "slimyenchants.basics.enchant") || SlimyEnchantsAPI.permissions.hasPermissions(((Player)sender), "slimyenchants.basics.*") || SlimyEnchantsAPI.permissions.hasPermissions(((Player)sender), "slimyenchants.*") || SlimyEnchantsAPI.permissions.hasPermissions(((Player)sender), "*")) 
						if (SlimyEnchantsAPI.getEnchantment(SlimyEnchantsAPI.capsFirstOnly(args[1])) != null) {
							SlimyEnchantsAPI.applyEnchant((Player) sender, ((Player) sender).getItemInHand(), SlimyEnchantsAPI.getEnchantment(SlimyEnchantsAPI.capsFirstOnly(args[1])), SlimyEnchantsAPI.getEnchantment(SlimyEnchantsAPI.capsFirstOnly(args[1])).getMaxLevel());
						} else {
							((Player)sender).sendMessage(sName + ChatColor.RED + " Enchantment," + ChatColor.LIGHT_PURPLE + SlimyEnchantsAPI.capsFirstOnly(args[1]) + ChatColor.RED + " ,does not exist!");
							if (!((Player)sender).getInventory().contains(SlimyEnchantsAPI.getEnchantsBooklet()))
								((Player)sender).getInventory().addItem(SlimyEnchantsAPI.getEnchantsBooklet());
							else ((Player)sender).sendMessage(sName + ChatColor.GREEN + "You already have book!");
						}
						
		return true;
	}

	private static StringBuilder help = null;	
	
	private void help(Player sender) {
		if (help != null)
			sender.sendMessage(help.toString());
		help = new StringBuilder();
//		if (SlimyEnchantsAPI.permissions.hasPermissions(((Player)sender), "slimyenchants.basics.enchant") || SlimyEnchantsAPI.permissions.hasPermissions(((Player)sender), "slimyenchants.basics.*") || SlimyEnchantsAPI.permissions.hasPermissions(((Player)sender), "slimyenchants.*") || SlimyEnchantsAPI.permissions.hasPermissions(((Player)sender), "*"))
		
		help.append(sName + ChatColor.GREEN + " help menu.\n");
		help.append(ChatColor.GOLD + "====================================\n");
		help.append("/slimyenchants\n");
		if (SlimyEnchantsAPI.permissions.hasPermissions(((Player)sender), "slimyenchants.basics.enchant") || SlimyEnchantsAPI.permissions.hasPermissions(((Player)sender), "slimyenchants.basics.*") || SlimyEnchantsAPI.permissions.hasPermissions(((Player)sender), "slimyenchants.*") || SlimyEnchantsAPI.permissions.hasPermissions(((Player)sender), "*"))
			help.append(ChatColor.GOLD + "  enchant" + ChatColor.WHITE + " -- " + ChatColor.AQUA + "Enchant the item held to MAX level\n");
		help.append(ChatColor.GOLD + "  book" + ChatColor.WHITE + " -- " + ChatColor.AQUA + "Get a book of all the enchantments\n");
		
		sender.sendMessage(help.toString());
	}

	public static Plugin getPlugin() {
		return plugin;
	}

}
